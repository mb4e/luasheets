
letters = "ABCDEFGHIJKLMNOPQRSTUWXYZ"

function set_cell(name,value)
    -- print(name,value,type(value))
    local value_tex = ""
    if type(value) == "number" then
        local x = value
        if cell_format == "percent" then x = 100*x end
        if cell_round then
            x = round(x,cell_round)
            cell_round = nil
        end
        if cell_format == "number" then
            value_tex = num(numbertostring(x))
            cell_format = nil
        elseif cell_format == "percent" then
            value_tex = percent(numbertostring(x))
            cell_format = nil
        elseif cell_format == "quantity" then
            if cell_unit then
                value_tex = qty(numbertostring(x),cell_unit)
                cell_unit = nil
            else
                value_tex = num(numbertostring(x))
            end
            cell_format = nil
        else
            value_tex = numbertostring(x)
        end
    else
        value_tex = tostring(value)
    end
    if cell_color then
        value_tex = string.format([[\textcolor{%s}{%s}]],cell_color,value_tex)
        cell_color = nil
    end
    if cell_phantom then
        value_tex = string.format([[\phantom{%s}]],value_tex)
        cell_phantom = nil
    end
    if cell_align then
        value_tex = string.format([[ \multicolumn{1}{%s|}{%s} ]],string.sub(cell_align,1,1),value_tex)
        cell_align = nil
    end
    _G[name] = value
    _G[name.."_tex"] = value_tex
    table.insert(worksheet,name)
end

function get_cell(name)
    return _G[name]
end

function get_cell_tex(name)
    return _G[name.."_tex"]
end

function get_row(i)
    local row = {}
    for k,v in pairs(worksheet) do
        if get_cell_i(v) == i then
            row[get_cell_j(v)] = get_cell_tex(v)
        end
    end
    return row
end

function get_cell_i(name)
    return tonumber(string.sub(name,2))
end

function get_cell_j(name)
    return string.find(letters,string.sub(name,1,1))
end

function nolabels(key)
    if key == "rows" or key == "all" then labelrows = false end
    if key == "columns" or key == "all" then labelcols = false end
end

function get_rows()
    local imax,jmax,rows = 1,1,{}
    for k,v in pairs(worksheet) do
        imax = math.max(imax,get_cell_i(v))
        jmax = math.max(jmax,get_cell_j(v))
    end
    for i=1,imax,1 do
        local row = get_row(i)
        for j=1,jmax,1 do
            if row[j] == nil then row[j] = "" end
        end
        -- print(table.concat(row," ; "))
        table.insert(rows,row)
    end
    return rows,imax,jmax
end

function row_string(row)
    return string.format([[ %s \\ ]],table.concat(row,[[ & ]]))
end

function print_row(first_cell,row,hline)
    if labelrows then
        tex.print(first_cell,[[ & ]],row_string(row),hline)
    else
        tex.print(row_string(row),hline)
    end
end

function init_worksheet()
    worksheet = {}
    labelrows = true
    labelcols = true
end

function print_worksheet()
    local closing
    local columns,hline = "",[[ \hline]]
    local rows,imax,jmax = get_rows()
    if not worksheet_font then worksheet_font = [[\ttfamily]] end
    tex.sprint(worksheet_font)
    if labelrows then
        columns = "c"
        hline = string.format([[ \cline{2-%s}]],jmax+1)
    end
    if worksheet_width then
        tex.print([[\newcolumntype{x}{>{\centering\arraybackslash}X}]])
        tex.print(string.format([[\begin{tabularx}{%s}{%s|*{%s}{x|}}]],worksheet_width,columns,jmax))
        columns = "x"
        closing = [[\end{tabularx}]]
    else
        tex.print(string.format([[\begin{tabular}{%s|*{%s}{c|}}]],columns,jmax))
        columns = "c"
        closing = [[\end{tabular}]]
    end
    if labelcols then
        local first_row = {}
        for j=1,jmax,1 do
            table.insert(first_row,string.format([[\multicolumn{1}{%s}{%s} ]],columns,string.sub(letters,j,j)))
        end
        print_row([[\multicolumn{1}{c}{} ]],first_row,hline)
    else
        tex.sprint(hline)
    end
    for i,row in pairs(rows) do
        print_row(tostring(i),row,hline)
    end
    tex.print(closing)
    clear_worksheet()
end

function clear_worksheet()
    worksheet = {}
    worksheet_width = nil
end

function numbertostring(x)
    return tostring(string.gsub(tostring(x),"%.%d*0+$",""))
end

function round(x,digits)
    return tonumber(string.format("%." .. (digits or 0) .. "f",x))
end

function num(x)
    if cell_siunitx then
        return string.format([[\num[%s]{%s}]],cell_siunitx,x)
    else
        return string.format([[\num{%s}]],x)
    end
end

function qty(x,u)
    if cell_siunitx then
        return string.format([[\qty[%s]{%s}{%s}]],cell_siunitx,x,u)
    else
        return string.format([[\qty{%s}{%s}]],x,u)
    end
end

function percent(x)
    if cell_siunitx then
        return string.format([[\qty[%s]{%s}{\percent}]],cell_siunitx,x)
    else
        return string.format([[\qty{%s}{\percent}]],x)
    end
end

function parse_cell_range(cell_range)
    local cells = {}
    local a,b = string.match(cell_range,"(%u%d+):(%u%d+)")
    local ai,aj = get_cell_i(a),get_cell_j(a)
    local bi,bj = get_cell_i(b),get_cell_j(b)
    for j=math.min(aj,bj),math.max(aj,bj) do
        for i=math.min(ai,bi),math.max(ai,bi) do
            cell = string.sub(letters,j,j) .. tostring(i)
            table.insert(cells,cell)
        end
    end
    return cells
end

function get_numbers(cell_range)
    local numbers = {}
    for k,name in pairs(parse_cell_range(cell_range)) do
        local v = get_cell(name)
        if type(v) == "number" then
            table.insert(numbers,v)
        end
    end
    return numbers
end

function sum_numbers(numbers)
    if #numbers > 0 then
        local s = 0
        for k,x in pairs(numbers) do
            s = s+x
        end
        return s
    end
end

function sum(cell_range)
    return sum_numbers(get_numbers(cell_range))
end

function mean_numbers(numbers)
    local s,n = sum_numbers(numbers),#numbers
    if s and n > 0 then
        return s/n
    end
end

function mean(cell_range)
    return mean_numbers(get_numbers(cell_range))
end

function min_numbers(numbers)
    local m
    for k,x in pairs(numbers) do
        if m == nil or x < m then
            m = x
        end
    end
    return m
end

function min(cell_range)
    return min_numbers(get_numbers(cell_range))
end

function max_numbers(numbers)
    local m
    for k,x in pairs(numbers) do
        if m == nil or x > m then
            m = x
        end
    end
    return m
end

function max(cell_range)
    return max_numbers(get_numbers(cell_range))
end

function range_numbers(numbers)
    local a,b
    for k,x in pairs(numbers) do
        if a == nil or x < a then
            a = x
        end
        if b == nil or x > b then
            b = x
        end
    end
    return b-a
end

function range(cell_range)
    return range_numbers(get_numbers(cell_range))
end

function median_numbers(numbers)
    local n = #numbers
    table.sort(numbers)
    if n%2 == 0 then
        return (numbers[n/2]+numbers[n/2+1])/2
    else
        return numbers[math.ceil(n/2)]
    end
end

function median(cell_range)
    return median_numbers(get_numbers(cell_range))
end

function var_numbers(numbers)
    local m,n = mean_numbers(numbers),#numbers
    if n > 0 then
        local s = 0
        for k,x in pairs(numbers) do
            s = s+(x-m)^2
        end
        return s/n
    end
end

function var(cell_range)
    return var_numbers(get_numbers(cell_range))
end

function stdev(cell_range)
    local v = var_numbers(get_numbers(cell_range))
    if v then
        return math.sqrt(v)
    end
end